//DANE

let positiveSundaysList = ['28.10.2018', '4.11.2018', '25.11.2018', '2.12.2018', '16.12.2018', '23.12.2018', '30.12.2018', '27.01.2019', '24.02.2019', '31.03.2019', '14.04.2019', '28.04.2019', '26.05.2019', '30.06.2019', '28.07.2019', '25.08.2019', '29.09.2019', '27.10.2019', '24.11.2019', '15.12.2019', '22.12.2019', '29.12.2019', '26.01.2020', '5.04.2020', '26.04.2020', '28.05.2020', '30.08.2020', '13.12.2020', '20.12.2020', '31.01.2021', '28.03.2021', '25.04.2021', '27.05.2021', '29.08.2021', '12.12.2021', '19.12.2021']

let sundaysList = []

//OPERACJE NA DATACH

function dateLiteralToDateObj(literal) {
    let array = literal.split('.')
    return date = new Date(array[2], array[1] - 1, array[0])
}

function closestSunday(today) {
    let daysToSunday = 7 - today.getDay()
    if (daysToSunday == 7) {
        daysToSunday = 0
    }
    let closest = new Date()
    closest.setDate(today.getDate() + daysToSunday)
    return closest
}

function arrayToObjs(array) {
    let arrayOfDateObjects = positiveSundaysList.map(dateLiteralToDateObj)
    return arrayOfDateObjects
}

function isSundayPositive(sunday, array) {
    for (let x = 0; x < array.length; x++) {
        if (sunday.getTime() === array[x].getTime()) {
            return "positive"
            break
        }
    }
    return "negative"
}

function nextSunday(sunday, today) {
    let result = new Date(today)
    result = sunday.setDate(sunday.getDate() + 7)
    let r = new Date(result)
    return r
}

function zeroHMS(date) {
    let result = date.setHours(0, 0, 0, 0)
    let r = new Date(result)
    return r
}

function monthToString(monthNumber) {
    let months = ['sty', 'lut', 'mar', 'kwi', 'maj', 'cze', 'lip', 'sie', 'wrz', 'paź', 'lis', 'gru']
    return months[monthNumber]
}

//GENEROWANIE GALERII

function generateGallery(obj) {
    let date = obj.date
    let state = obj.state

    var galleryItem = document.createElement("div")
    galleryItem.setAttribute("class", "gallery-item " + state)
    gallery.appendChild(galleryItem)

    var galleryItemDay = document.createElement("div")
    galleryItemDay.setAttribute("class", "gallery-item-day font")
    galleryItemDay.innerHTML = date.getDate().toString()
    galleryItem.appendChild(galleryItemDay)

    var galleryItemMonth = document.createElement("div")
    galleryItemMonth.setAttribute("class", "gallery-item-month font")
    galleryItemMonth.innerHTML = monthToString(date.getMonth())
    galleryItem.appendChild(galleryItemMonth)

    var galleryItemYear = document.createElement("div")
    galleryItemYear.setAttribute("class", "gallery-item-year font")
    galleryItemYear.innerHTML = date.getFullYear().toString()
    galleryItem.appendChild(galleryItemYear)

}

//EGZEKUCJA

positiveSundaysList = arrayToObjs(positiveSundaysList)

let today = new Date()
let nextSun = closestSunday(today)

var gallery = document.createElement("div")
gallery.setAttribute("class", "gallery")
document.body.appendChild(gallery)

var reference = document.getElementById('P')
reference.after(gallery)

for (let i = 0; nextSun.getFullYear() < 2020; i++) {
    nextSun = zeroHMS(nextSun)
    let objct = {
        date: nextSun,
        state: isSundayPositive(nextSun, positiveSundaysList)
    }

    sundaysList.push(objct)


    console.log(nextSun)
    generateGallery(objct)
    const bgColor = document.querySelector('.gallery-item')
    if (objct.state == true) {

    }
    nextSun = nextSunday(nextSun)
}

//STEROWANIE GALERIĄ

const _Gallery = document.querySelector('.gallery'),
    galleryLenght = _Gallery.children.length;

_Gallery.style.setProperty('--numberOfItems', galleryLenght)

let itemIndex = 0

let setItemIndex = function (index) {
    if (index < 0) {
        index = 0
    } else {
        if (index > galleryLenght - 1) {
            index = galleryLenght - 1
        }

    }
    return index
}

let forward = function () {
    itemIndex = setItemIndex(++itemIndex)
    update()
}

let backward = function () {
    itemIndex = setItemIndex(--itemIndex)
    update()
}

let update = function () {
    _Gallery.style.setProperty('--indexOfSelectedItem',
        setItemIndex(itemIndex))
}

document.getElementById("L").onclick = function () {
    backward()
};

document.getElementById("P").onclick = function () {
    forward()
};
