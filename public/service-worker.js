self.addEventListener('install', function(e) {
 e.waitUntil(
   caches.open('jakaniedziela').then(function(cache) {
     return cache.addAll([
       '/',
       '/index.html',
       '/index.js',  
       '/main.css'
     ]);
   })
 );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
      })
    );
});